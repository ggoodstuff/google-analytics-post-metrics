<?php
/**
 * Register meta box(es).
 */
function ga_post_pageviews_register_meta_boxes() {
	add_meta_box( 'meta-box-id', __( 'Google Analytics', 'textdomain' ), 'ga_post_pageviews_display_callback', array(
		'post',
		'page'
	), 'normal', 'high' );
}

add_action( 'add_meta_boxes', 'ga_post_pageviews_register_meta_boxes' );

function averageArraySegment( $a ) {
	$sum = 0;
	foreach ( $a as $i ) {
		$sum += $i;
	}

	return $sum / ( count( $a ) ? count( $a ) : 1 );
}

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function ga_post_pageviews_display_callback( $post ) {
	// Display code/markup goes here. Don't forget to include nonces!

	?>

    <style>
        .sparkline {
            width: 100%;
            height: 3em;
        }
    </style>

	<?php

	$ga_stats = gapp_get_post_pageviews( $post->ID, true, true );

	$revisions = wp_get_post_revisions( $post );
	$revisions = array_reverse( $revisions );

	$segments = [];

	foreach ( $revisions as $rev ) {
		$segments[] = array( "start" => intval( date( 'Ymd', strtotime( $rev->post_modified ) ) ) );
	}
	for ( $i = 0; $i < count( $segments ) - 1; $i ++ ) {
		$segments[ $i ]['end'] = $segments[ $i + 1 ]['start'];
	}

	if ( ! isset( $segments[ count( $segments ) - 1 ]['end'] ) ) {
		$segments[ count( $segments ) - 1 ]['end'] = intval( date( 'Ymd' ) );
	}

	echo "<table>";

	echo "<tr><td width='25%'>revision</td><td></td><td>views</td><td></td><td></td><td>bounce</td><td></td><td></td><td>exit</td><td></td></tr>";
	foreach ( $segments as $seg ) {
		$sets = array( "views" => [], "exit" => [], "bounce" => [] );

		for ( $i = 0; $i < count( $ga_stats['day'] ); $i ++ ) {

			if ( $ga_stats['day'][ $i ] > $seg["start"] && $ga_stats['day'][ $i ] <= $seg["end"] ) {
				$sets["views"][]  = $ga_stats['views'] [ $i ];
				$sets["bounce"][] = $ga_stats['bounce'] [ $i ];
				$sets["exit"][]   = $ga_stats['exit'] [ $i ];
			}

		}

		if ( strtotime( $seg['end'] ) - strtotime( $seg['start'] ) > 24 ) {
			echo "<tr>";
			print_r( '<td>' . $seg['start'] . " - " . $seg['end'] . "</td>" );

			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["views"], 0, intval(count( $sets["views"] )/2) ) ) ,2) . "</td>";
			echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . ( join( ",", $sets["views"] ) ) . "' >canvas</canvas></td>";
			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["views"], count( $sets["views"] ) - 6, intval(count( $sets["views"] )/2) ) ) ,2) . "</td>";

			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["bounce"], 0, intval(count( $sets["bounce"] )/2) ) ) ,2) . "</td>";
			echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . ( join( ",", $sets["bounce"] ) ) . "' >canvas</canvas></td>";
			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["bounce"], count( $sets["bounce"] ) - 6, intval(count( $sets["bounce"] )/2) ) ) ,2) . "</td>";

			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["exit"], 0, intval(count( $sets["exit"] )/2) ) ) ,2) . "</td>";
			echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . ( join( ",", $sets["exit"] ) ) . "' >canvas</canvas></td>";
			echo "<td>" . number_format( averageArraySegment( array_slice( $sets["exit"], count( $sets["exit"] ) - 6, intval(count( $sets["exit"] )/2) ) ) ,2) . "</td>";

			echo "</tr>";

		}

	}
	echo "</table>";

        echo "<table style='width:100%'>";
        echo "<tr><td><h3>Pageviews</h3></td>";
        echo "<td><h3>" . number_format($ga_stats["total"]->{"ga:pageviews"},2) . "</h3></td>";
        echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . (join(",", $ga_stats['views'])) . "' >canvas</canvas></td>";
        echo "</tr>";

        echo "<tr><td><h3>Bounce Rate</h3></td>";
        echo "<td><h3>" . number_format($ga_stats["total"]->{"ga:bounceRate"},2) . "%</h3></td>";
        echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . (join(",", $ga_stats['bounce'])) . "' >canvas</canvas></td>";
        echo "</tr>";

        echo "<tr><td><h3>Exit</h3></td>";
        echo "<td><h3>" . number_format($ga_stats["total"]->{"ga:exitRate"},2) . "%</h3></td>";
        echo "<td><canvas class='sparkline' width='600px' height='100px' data-sparkline='" . (join(",", $ga_stats['exit'])) . "' >canvas</canvas></td>";
        echo "</tr>";
        echo "</table>";


    //echo "<h3>Pageviews: " . ( $ga_stats["total"]->{"ga:pageviews"} ) . "</h3>";
	//echo "<h3>Bounce Rate: " . ( $ga_stats["total"]->{"ga:bounceRate"} ) . "%</h3>";
	//echo "<h3>Exit: " . ( $ga_stats["total"]->{"ga:exitRate"} ) . "%</h3>";


	?>
    <script>

        /* Usage:
		<canvas class="sparkline" data-sparkline="1,2,7,5,2,9,10,3,7,8">Your browser does not support the HTML5 canvas
			tag.
		</canvas> */
        /* Where data-sparkline is a list of values representing the data to be displayed. */

        function max(array) {
            return Math.max.apply(Math, array);
        }

        function sparkline(obj) {
            var c = obj;
            var ctx = c.getContext("2d");
            var data = c.getAttribute("data-sparkline");
            var spark = data.split(',');
            for (a in spark) {
                spark[a] = parseInt(spark[a], 10);
            }
            var margin = 20;
            var ratioW = ((c.width - margin * 2) * 1) / spark.length;
            var ratioH = ((c.height - margin * 2) * .8) / Math.max.apply(Math, spark);

            var x = 0;
            var y = 0;
            var grad = ctx.createLinearGradient(0, 0, c.width, c.height);
            grad.addColorStop(0, "#007AC9");  // Initial path colour
            grad.addColorStop(1, "#00c972");  // End stroke colour

            ctx.strokeStyle = grad;
            ctx.fillStyle = grad;

            ctx.beginPath();
            ctx.lineWidth = "2";
            ctx.arc(margin, c.height - (spark[0] * ratioH + margin), 2, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
            for (index in spark) {
                if (index == 0) {
                    // First time
                    ctx.beginPath();
                    ctx.lineWidth = "2";
                    ctx.moveTo(margin, c.height - (spark[index] * ratioH + margin));
                } else {
                    x = index * ratioW + margin;
                    y = c.height - (spark[index] * ratioH + margin);
                    ctx.lineTo(x, y);
                }
            }
            ctx.stroke();

            ctx.beginPath();
            ctx.lineWidth = "2";
            ctx.arc(x, y, 2, 0, 2 * Math.PI);
            ctx.fill();
            ctx.stroke();
        }

        var _sparklines_ = document.querySelectorAll('.sparkline');
        for (i = 0; i < _sparklines_.length; i++) {
            sparkline(_sparklines_[i]);
        }

    </script>
	<?php

}